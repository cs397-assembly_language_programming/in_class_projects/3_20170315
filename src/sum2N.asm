;++++++++++++++++++++++++++++
;=======================
; Problem: 
;  ask user for N;
;    Repeat N (times):
;          ask user for 2 numbers
;          add them,
;          then display result
;   
Include Irvine32.inc
.data
  xVal DWORD ?
  yVal DWORD ?
  zVal DWORD ?

prompt BYTE "Please input value: ",0
resultmsg BYTE "Result is: ",0
promptN BYTE "Number times to Repeat: ",0

.code
 Main PROC



   Call Clrscr
   Call Crlf;
   Call Crlf;

;  ask user for N;;
    MOV EDX, OFFSET promptN
    Call WriteString
    Call ReadDec

;    Repeat N (times):

    MOV ECX, EAX;  set counter with
;                ; user-supplied value
;
;          ask user for 2 numbers
;          add them,
;          then display result

   Call Crlf
   Call Crlf

L1:
;  ask user for xval & store result
MOV EDX, OFFSET prompt
Call WriteString
  ;
Call ReadDec; value returned in EAX
MOV xVal, EAX

Call Crlf
;  ask user for yval & store result
MOV EDX, OFFSET prompt
Call WriteString

Call ReadDec; value returned in EAX
MOV yVal, EAX

;  add xval & yval, store as zval
ADD EAX, xval
MOV zVal, EAX

Call Crlf

;  display zval
; WriteDec: pass the value to be displayed
; in EAX

MOV EDX, OFFSET resultmsg
Call WriteString

Call WriteDec;

LOOP L1

exit
Main ENDP
END Main